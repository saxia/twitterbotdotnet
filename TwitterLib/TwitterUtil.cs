﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TweetSharp;
using System.Diagnostics;
using System.IO;

namespace TwitterLib
{
    public class TwitterUtil
    {
        const string filePath = "OAuth.dat";

        const string key = "RSOKUocNWzt3WU2AYJIMwY0LG";
        const string sec = "mVO5wKfOiRLrsLKl2sl8BEO2QdLD8MS43OQEnsHAZJSKiml05i";

        TwitterService service;
        OAuthRequestToken request_token;

        public bool Initialize()
        {
            service = new TwitterService(key, sec);
            if (!File.Exists(filePath))
            {
                request_token = service.GetRequestToken();
                Uri uri = service.GetAuthorizationUri(request_token);
                Process.Start(uri.ToString());
                return false;
            }

            StreamReader reader = new StreamReader(filePath);
            var Token = reader.ReadLine();
            var TokenScret = reader.ReadLine();
            reader.Close();
            service.AuthenticateWith(Token, TokenScret);
            return true;
        }

        public void OAuthStart(string code)
        {
            OAuthAccessToken access = service.GetAccessToken(request_token, code);
            service.AuthenticateWith(access.Token, access.TokenSecret);

            StreamWriter writer = new StreamWriter(filePath);
            writer.WriteLine(access.Token);
            writer.WriteLine(access.TokenSecret);
            writer.Close();
        }

        public void SendTweet(string text)
        {
            var stat = service.SendTweet(new SendTweetOptions { Status = text });
        }

        public void SendReply(string text,long id)
        {
            var stat = service.SendTweet(new SendTweetOptions { Status = text, InReplyToStatusId = id });
        }

        /// <summary>
        /// 自分のTLとってくる
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public IEnumerable<TwitterStatus> GetTL(int num)
        {
            return service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions { Count = num });
        }

        /// <summary>
        /// 特定ユーザーのツイートを取得する。
        /// </summary>
        /// <param name="name"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public IEnumerable<TwitterStatus> GetUserTweet(string name ,int num)
        {
            
            var option = new ListTweetsOnUserTimelineOptions();
            option.ScreenName = name;
            option.Count = num;
            return service.ListTweetsOnUserTimeline(option);
        }

        /// <summary>
        /// 自分への返答を取得する。
        /// </summary>
        /// <param name="num"></param>
        /// <param name="segment_time"></param>
        /// <returns></returns>
        public IEnumerable<TwitterStatus> GetMentioningMe(int num, DateTime segment_time)
        {
            var list = service.ListTweetsMentioningMe(new ListTweetsMentioningMeOptions { Count = num });
            return  list.Where(x => x.CreatedDate > segment_time);
        }


        /// <summary>
        /// 自分のTLから特定の単語を抽出する。
        /// </summary>
        /// <param name="search_words"></param>
        /// <param name="num"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        public IEnumerable<TwitterStatus> GetFollowerWord(List<string> search_words ,int num ,DateTime now)
        {
            var tl = GetTL(num);
            return tl.Where(x => x.CreatedDate > now && search_words.Any(y => x.Text.IndexOf(y) >= 0));   
        }
    }
}
