﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TweetSharp;

namespace TwitterLib
{
    public delegate string GetAutoTweet();
    public delegate IEnumerable<string> GetTimerTweet(DateTime now);
    public delegate string GetReplyString(TwitterStatus rep);
    public delegate string GetSearchReplyString(string find_word, TwitterStatus rep);

    public class TweetBot
    {
        TwitterUtil tweeter_util;

        // 認証してから引き渡すこと。
        public TweetBot(TwitterUtil util)
        {
            tweeter_util = util;
        }

        /// <summary>
        /// 前回の自動ツイート時間
        /// </summary>
        static DateTime previousTweetTime;
        /// <summary>
        /// 自動ツイート
        /// </summary>
        /// <param name="autotweet"></param>
        /// <param name="now"></param>
        /// <param name="ts"></param>
        public void AutoTweet(GetAutoTweet autotweet, DateTime now, TimeSpan ts)
        {
            if ((now - previousTweetTime) < ts) { return; }
            var auto_tweet_text = autotweet();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(auto_tweet_text);
            Console.ResetColor();
            tweeter_util.SendTweet(auto_tweet_text);
            previousTweetTime = now;
        }

        /// <summary>
        /// 特定時間にツイート
        /// </summary>
        /// <param name="func"></param>
        /// <param name="now"></param>
        public void TimerTweet(GetTimerTweet func, DateTime now)
        {
            var tweets = func(now);
            if (!tweets.Any()) { return; }
            foreach (var tweet in tweets)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(tweets);
                Console.ResetColor();
                tweeter_util.SendTweet(tweet);
            }
        }

        /// <summary>
        /// 検索して応答
        /// </summary>
        /// <param name="func"></param>
        /// <param name="list"></param>
        /// <param name="now"></param>
        public void AutoSearchWordsTweet(GetSearchReplyString func, List<string> list, DateTime now)
        {
            var find = tweeter_util.GetFollowerWord(list, 10, now);
            if (!find.Any()) { return; }
            foreach (var find_stat in find)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(find_stat.User.Name);
                Console.ResetColor();
                Console.WriteLine(find_stat.Text);
                //tweeter_util.SendTweet(func(list.First(x => find_stat.Text.IndexOf(x) >= 0), find_stat));
                tweeter_util.SendReply(func(list.First(x => find_stat.Text.IndexOf(x) >= 0), find_stat), find_stat.Id);
            }
        }

        /// <summary>
        /// 自動応答
        /// </summary>
        /// <param name="reply_pattern"></param>
        /// <param name="now"></param>
        public void AutoReply(GetReplyString get_reply_func, DateTime now)
        {
            // なけりゃ返さない。
            var rep_list = tweeter_util.GetMentioningMe(50, now);
            if (!rep_list.Any()) { return; }
            foreach (var rep_stat in rep_list)
            {
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine(rep_stat.User.Name);
                Console.ResetColor();
                Console.WriteLine(rep_stat.Text);
                tweeter_util.SendTweet(get_reply_func(rep_stat));
            }
        }
    }
}
