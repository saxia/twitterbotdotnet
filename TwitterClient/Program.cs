﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using TwitterLib;
using System.Threading;

namespace TwitterClient
{
    class Program
    {
        const int timespan = 2;

        static void Main(string[] args)
        {
            var twitterUtil = new TwitterUtil();
            if (!twitterUtil.Initialize())
            {
                Console.WriteLine("PINキーの入力");
                twitterUtil.OAuthStart(Console.ReadLine());
            }
            var bot = new TweetBot(twitterUtil);
            var model = new TwitterBotModel.BotModel();
            while (true)
            {
                bot.AutoTweet(model.AutoTweet, DateTime.Now, TimeSpan.FromMinutes(30));
                bot.TimerTweet(model.TimerTweet, DateTime.Now);

                if (DateTime.Now.Minute % timespan == 0 && DateTime.Now.Second <= 0)
                {
                    //ClientCommand(twitterUtil);
                    bot.AutoReply(model.Reply, DateTime.UtcNow.AddMinutes(-1 * (timespan)));
                    bot.AutoSearchWordsTweet(model.SearchReplyTweet, new List<string> { "ぬるぽ" }, DateTime.UtcNow.AddMinutes(-1 * (timespan)));
                }
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// コマンドにてテスト用
        /// </summary>
        /// <param name="twitterUtil"></param>
        private static void ClientCommand(TwitterUtil twitterUtil)
        {
            var text = Console.ReadLine();
            Console.Clear();

            ComandLineParser.Parse(text.Split(','));
            if (ComandLineParser.SEND != string.Empty &&
                ComandLineParser.SEND != null)
            {
                twitterUtil.SendTweet(ComandLineParser.SEND);
            }

            if (ComandLineParser.READ > 0)
            {
                foreach (var tweet in twitterUtil.GetFollowerWord(new List<string> { "昼", "SDVX" }, 10, DateTime.UtcNow.AddMinutes(-2)))
                //foreach (var tweet in twitterUtil.GetMentioningMe(ComandLineParser.READ , DateTime.Now.AddDays(-2)))
                {
                    Console.WriteLine(tweet.User.Name);
                    Console.WriteLine(tweet.Text);
                }
                Console.WriteLine("");
            }

            if (ComandLineParser.REPLY > 0)
            {
                foreach (var tweet in twitterUtil.GetUserTweet("saxia", ComandLineParser.REPLY))
                {
                    Console.WriteLine(tweet.User.Name);
                    Console.WriteLine(tweet.Text);
                }
                Console.WriteLine("");
            }
        }
    }
}
