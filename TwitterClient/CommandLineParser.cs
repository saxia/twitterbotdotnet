﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitterClient
{
    class ComandLineParser
    {
        static HashSet<string> options = new HashSet<string> { "-s", "-r" , "-u" };

        static Dictionary<string, string> parse_data;

        public static void Parse(string[] args)
        {
            string key = string.Empty;
            parse_data = args.GroupBy(s => options.Contains(s) ? key = s : key).ToDictionary(g => g.Key, g => g.Skip(1).FirstOrDefault());
        }

        static public string SEND
        {
            get { try { return parse_data["-s"]; } catch { return string.Empty; } }
        }

        static public int READ
        {
            get { try { return int.Parse(parse_data["-r"]); } catch { return 0; } }
        }

        static public int REPLY
        {
            get { try { return int.Parse(parse_data["-u"]); } catch { return 0; } }
        }
    }
}
