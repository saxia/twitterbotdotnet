﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NMeCab;

namespace TwitterBotModel
{
    public class TweetTextParse
    {
        public static DateTime TimerParse(string text)
        {
            var parseBaes = text.Replace("時限", "xxxx"); //まずコマンドを置き換え

            DateTime setting_time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            var day = GetTimeFromString(parseBaes, "日", DateTime.Now.Day);
            var hour = GetTimeFromString(parseBaes, "時", 0);
            var min = GetTimeFromString(parseBaes, "分", 0);
            var sec = GetTimeFromString(parseBaes, "秒", 0);
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, day, hour, min, sec);
        }

        public static string ParseBracket(string text)
        {
            var pattern = "「.*」";
            if (Regex.IsMatch(text, pattern))
            {
                return Regex.Match(text, pattern).Value;
            }
            return string.Empty;
        }


        private static int GetTimeFromString(string parseBaes, string japanese_text, int base_num)
        {
            var pattern = "\\d{1,2}" + japanese_text;
            if (Regex.IsMatch(parseBaes, pattern))
            {
                return int.Parse(Regex.Match(Regex.Match(parseBaes, pattern).Value, "\\d{1,2}").Value);
            }
            return base_num;
        }

        public static List<string> GetNouns(string text)
        {
            return Morphological_Analysis(text, "名詞");
        }

        public static List<string> GetVerb(string text)
        {
            return Morphological_Analysis(text, "動詞");
        }

        private static List<string> Morphological_Analysis(string text, string part)
        {
            MeCabParam param = new MeCabParam();
            param.DicDir = @"C:\Users\saxia.SAXIA\Documents\Visual Studio 2010\Projects\TwitterClient\TwitterClient\TwitterClient\bin\Debug\dic\ipadic";

            MeCabTagger t = MeCabTagger.Create(param);
            MeCabNode node = t.ParseToNode(text);

            var ret = new List<string>();

            while (node != null)
            {
                if (node.CharType > 0 && node.Feature.IndexOf(part) >= 0)
                {
                    //Console.WriteLine(node.Surface + "\t" + node.Feature);
                    ret.Add(node.Surface);
                }
                node = node.Next;
            }
            return ret;
        }
    }
}
