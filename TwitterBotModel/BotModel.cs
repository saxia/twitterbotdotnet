﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TweetSharp;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Xml;
using System.ServiceModel.Syndication;
using NMeCab;

namespace TwitterBotModel
{
    public class BotModel
    {
        class TimerDataContainer
        {
            public string user { get; set; }
            public DateTime time { get; set; }
            public string text { get; set; }
        }

        List<TimerDataContainer> timerContainer;

        const string default_format = "@{0} 返答ありがとう(*´ω｀*) {1}";

        List<string> ward_list;
        public BotModel()
        {
            ward_list = new List<string>();
            ward_list.Add("ぬるぽ");
            ward_list.Add("何時");
            ward_list.Add("画像検索");
            ward_list.Add("時限");
            ward_list.Add("近くの");
            ward_list.Add("ニュース");
            timerContainer = new List<TimerDataContainer>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        public string Reply(TwitterStatus ts)
        {
            var txt = ts.Text;

            var Nounslist = TweetTextParse.GetNouns(txt);
            var Verblist = TweetTextParse.GetVerb(txt);

            if (Nounslist.Intersect(new List<string> { "画像", "検索" }).Count() == 2)
            {
                return SearchImg(ts);
            }

            var find = ward_list.FirstOrDefault(x => txt.IndexOf(x) >= 0);
            switch (find)
            {
                case "ぬるぽ":
                    return string.Format("@{0} がっ!", ts.User.ScreenName);
                case "何時":
                    return string.Format("@{0} 只今{1}をお知らせします。", ts.User.ScreenName, DateTime.Now.ToString());
                case "時限":
                    return TimerSetting(ts);
                case "画像検索":
                    return SearchImg(ts);
                case "近くの":
                    return SearchMap(ts);
                case "ニュース":
                    return SearchNews(ts);
                default:
                    return ReplyDefault(ts);
            }
        }

        private static string SearchNews(TwitterStatus ts)
        {
            var parsebase = ts.Text.Replace("ニュース", ",");
            var array = parsebase.Split(',');

            var ward = string.Empty;
            if (array.Count() > 1)
            {
                ward = array[1];
            }
            var rss_url = string.Format("https://news.google.com/news/feeds?hl=ja&ned=us&ie=UTF-8&oe=UTF-8&output=rss&q={0}", ward);
            var feed = WebServiseUtil.GetRSS(rss_url);
            var max = feed.Items.Count();
            var idx = new Random().Next(0, (max - 1));
            var view = feed.Items.ElementAtOrDefault(idx);
            return string.Format("@{0} {1} {2}", ts.User.ScreenName, view.Title.Text, WebServiseUtil.getShortURL(view.Links.FirstOrDefault().Uri.ToString()));

        }

        private static string SearchMap(TwitterStatus ts)
        {
            var parsebase = ts.Text.Replace("近くの", ",");
            var array = parsebase.Split(',');
            try
            {
                var create_url = string.Format("https://www.google.co.jp/maps/search/{0}/@{1},{2}", array[(array.Count() - 1)], ts.Location.Coordinates.Latitude, ts.Location.Coordinates.Longitude);
                return string.Format("@{0} 近くの{2}を検索したよ! {1}", ts.User.ScreenName, WebServiseUtil.getShortURL(create_url), array[(array.Count() - 1)]);
            }
            catch
            {
                var create_url = string.Format("https://www.google.co.jp/maps/search/{0}", array[(array.Count() - 1)]);
                return string.Format("@{0} 近くの{2}を検索したよ! {1}", ts.User.ScreenName, WebServiseUtil.getShortURL(create_url), array[(array.Count() - 1)]);
            }
        }

        /// <summary>
        /// デフォルトのリプライ
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        private static string ReplyDefault(TwitterStatus ts)
        {
            var array = ts.Text.Split(' ');
            return string.Format(default_format, ts.User.ScreenName,
                WebServiseUtil.getShortURL(string.Format("https://www.google.co.jp/search?q={0}", array.LastOrDefault())));
        }

        /// <summary>
        /// 画像検索
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        private static string SearchImg(TwitterStatus ts)
        {
            var array = ts.Text.Split(' ');
            return string.Format("@{0} {1}", ts.User.ScreenName,
                WebServiseUtil.getShortURL(string.Format("https://www.google.co.jp/search?q={0}&tbm=isch", array.LastOrDefault(x => x != "画像検索"))));
        }

        /// <summary>
        /// タイマー設置
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        private string TimerSetting(TwitterStatus ts)
        {
            DateTime setting_time = TweetTextParse.TimerParse(ts.Text);
            var reason = TweetTextParse.ParseBracket(ts.Text);

            string rep_text;
            if (reason == string.Empty)
            {
                reason = "タイマー";
                rep_text = "予定を確認してください!";
            }
            else { rep_text = string.Format("{0}のお時間です!", reason); }

            timerContainer.Add(new TimerDataContainer
            {
                user = ts.User.ScreenName,
                text = string.Format("{0}さん {1}になりました!{2}", ts.User.Name, setting_time, rep_text),
                time = setting_time
            }
                );

            return string.Format("@{0} {1}に{2}を設定しました。", ts.User.ScreenName, setting_time.ToString(), reason);
        }

        /// <summary>
        /// 時限式ツイート
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public IEnumerable<string> TimerTweet(DateTime now)
        {
            var list = from x in timerContainer
                       where now.AddMinutes(-1) < x.time && x.time <= now
                       select string.Format("@{0} {1}", x.user, x.text);

            //削除前に一度メモリに展開
            var ret = list.ToList();
            //削除
            timerContainer.RemoveAll(x => x.time <= now);
            return ret;
        }

        /// <summary>
        /// 自動ツイート
        /// </summary>
        /// <returns></returns>
        public string AutoTweet()
        {
            var feed = WebServiseUtil.GetRSS("http://rss.dailynews.yahoo.co.jp/fc/rss.xml");

            var max = feed.Items.Count();
            var idx = new Random().Next(0, (max - 1));
            var view = feed.Items.ElementAtOrDefault(idx);
            return string.Format("{0} {1}", view.Title.Text, WebServiseUtil.getShortURL(view.Links.FirstOrDefault().Uri.ToString()));
        }


        public string SearchReplyTweet(string find_word, TwitterStatus rep)
        {
            switch (find_word)
            {
                case "ぬるぽ":
                    return string.Format("@{0} がっ!", rep.User.ScreenName);

                default:
                    return string.Empty;
            }
        }
    }
}
