﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.ServiceModel.Syndication;
using System.Xml;

namespace TwitterBotModel
{
    public class WebServiseUtil
    {
        //{"kind": "urlshortener#url","id": "http://goo.gl/stTXr","longUrl": "http://www.ochi-lab.org/"}
        [DataContract]
        public class ShortURLInfo
        {
            [DataMember]
            public string kind { get; set; }
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public string longUrl { get; set; }
        }

        static public string getShortURL(string url)
        {
            string serviceURL = "https://www.googleapis.com/urlshortener/v1/url";
            string jsonRequest = "{\"longUrl\": \"" + url + "\"}";
            WebClient client = new WebClient { Encoding = Encoding.UTF8 };
            client.Headers["Content-Type"] = "application/json";
            var ret = client.UploadString(serviceURL, jsonRequest);
            client.Dispose();

            var serializer = new DataContractJsonSerializer(typeof(ShortURLInfo));
            var jsonBytes = Encoding.Unicode.GetBytes(ret);
            var sr = new MemoryStream(jsonBytes);

            var obj = (ShortURLInfo)serializer.ReadObject(sr);
            return obj.id;
        }

        static public SyndicationFeed GetRSS(string url)
        {
            SyndicationFeed feed;
            using (XmlReader rdr = XmlReader.Create(url))
            {
                feed = SyndicationFeed.Load(rdr);
            }
            return feed;
        }
    }
}
